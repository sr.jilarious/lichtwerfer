extern crate sdl2; 
extern crate cgmath;

use std::fmt;
// use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::pixels::PixelFormatEnum;
use sdl2::rect::Rect;
use sdl2::keyboard::Keycode;
use std::time::Duration;

//use crate::lichtwerfer::*;
use cgmath::prelude::*;

use console::Term;

// pub mod lichtwerfer
// {
//     extern crate cgmath;
//     use cgmath::prelude::*;
    
    #[derive(Clone)]
    pub struct Ray {
        origin: cgmath::Vector3<f64>,
        direction: cgmath::Vector3<f64>,
    }

    impl fmt::Display for Ray {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "Ray {{ origin: {{x: {}, y: {}, z: {} }}, direction: {{ x:{}, y:{}, z:{}}}", self.origin.x, self.origin.y, self.origin.z, 
            self.direction.x, self.direction.y, self.direction.z)
        }
    }

    #[derive(Clone, Debug)]
    pub struct Color {
        r: f64,
        g: f64,
        b: f64
    }


    #[derive(Clone, Debug)]
    pub struct Material {
        diffuse: Color,
        specular: Color,
        ambient: Color,
        spec_amount: f64,
    }

    #[derive(Clone, Debug)]
    pub struct Sphere {
        origin: cgmath::Vector3<f64>,
        radius: f64,
        radius_squared: f64,
        matIdx: usize
    }

    #[derive(Clone, Debug)]
    pub struct Light {
        position: cgmath::Vector3<f64>,
        intensity: f64
    }

    impl Sphere {
        pub fn new(origin: cgmath::Vector3<f64>, radius: f64, matIdx: usize) -> Sphere  
        {
            Sphere {
                origin: origin,
                radius: radius,
                radius_squared: radius * radius,
                matIdx: matIdx
            }
        }
    }

    #[derive(Clone, Debug)]
    pub struct Plane {
        origin: cgmath::Vector3<f64>,
        up: cgmath::Vector3<f64>,
        extent: f64,
    }
//}

#[derive(Clone)]
pub struct HitInfo {
    t: f64,
    reflect: Ray,
}

// fn intersect(ray : &Ray, sphere: &Sphere) -> Option<HitInfo> {

//     // const Vector displacement = vantage - Center();
//     // const double a = direction.MagnitudeSquared();
//     // const double b = 2.0 * DotProduct(direction, displacement);
//     // const double c = displacement.MagnitudeSquared() - radius*radius;
//     // const double radicand = b*b - 4.0*a*c;

//     let L = (sphere.origin - ray.origin);

//     let a = ray.direction.magnitude2();
//     let b = 2.0 * ray.direction.dot(L);
//     let c = L.magnitude2() - sphere.radius_squared;
//     let radicand = b*b - 4.0*a*c;

//     // let tc = L.dot(ray.direction);

//     if radicand < 0.0 {
//         return None;
//     }

//     // let d2 = tc*tc - L.magnitude2();

//     // //println!("radius^2 = {}, d2 = {}", sphere.radius_squared, d2);
//     // if d2 > sphere.radius_squared {
//     //     return None;
//     // }

//     // let t1c = (sphere.radius_squared - d2).sqrt();
//     let tc = radicand.sqrt();
    
//     let mut t0 = ((-b - tc)/(2.*a));
//     let mut t1 = ((-b + tc)/(2.*a));
//     //let t1 = t1c + tc;

    
//     if t1 > 0. || t0 > 0. {
//         t0 = t0.max(0.);
//         t1 = t1.max(0.);
//         if t0 <= t1 {
//             let intersect_point = ray.origin + t0 * ray.direction;
//             Some(HitInfo { 
//                 t: t0,
//                 reflect: Ray { 
//                     origin: intersect_point,
//                     direction: (intersect_point - sphere.origin).normalize()
//                 } })
//         }
//         else {
//             let intersect_point = ray.origin + t1 * ray.direction;
//             Some(HitInfo { 
//                 t: t1,
//                 reflect: Ray { 
//                     origin: intersect_point,
//                     direction: (intersect_point - sphere.origin).normalize(),
//                 } })
//         }
//     }
//     else {
//         None
//     }
// }

fn intersect(ray : &Ray, sphere: &Sphere) -> Option<HitInfo> {

    let L = sphere.origin - ray.origin; 
    let tca = L.dot(ray.direction); 
    if (tca < 0.) {
        return None; 
    }
    
    let d2 = L.dot(L) - tca * tca; 
    if (d2 > sphere.radius_squared) {
        return None; 
    }

    let thc = (sphere.radius_squared - d2).sqrt();
    let mut t0 = tca - thc; 
    let mut t1 = tca + thc; 

    if t1 > 0. || t0 > 0. {
        t0 = t0.max(0.);
        t1 = t1.max(0.);
        if t0 <= t1 {
            let intersect_point = ray.origin + t0 * ray.direction;
            Some(HitInfo { 
                t: t0,
                reflect: Ray { 
                    origin: intersect_point,
                    direction: (intersect_point - sphere.origin).normalize()
                } })
        }
        else {
            let intersect_point = ray.origin + t1 * ray.direction;
            Some(HitInfo { 
                t: t1,
                reflect: Ray { 
                    origin: intersect_point,
                    direction: (intersect_point - sphere.origin).normalize(),
                } })
        }
    }
    else {
        None
    }
}

const IMAGE_WIDTH : f64 = 800.;
const IMAGE_HEIGHT : f64 = 600.;
const IMAGE_LEFT : f64 = -IMAGE_WIDTH/2.;
const IMAGE_RIGHT : f64 = IMAGE_WIDTH/2.;

const IMAGE_TOP : f64 = IMAGE_HEIGHT/2.;
const IMAGE_BOTTOM : f64 = -IMAGE_HEIGHT/2.;

pub fn camera_ray(x:usize, y:usize) -> Ray {
    let invWidth = 1. / IMAGE_WIDTH as f64;
    let invHeight = 1. / IMAGE_HEIGHT as f64; 
    let fov = 30.;
    let aspectratio = IMAGE_WIDTH / IMAGE_HEIGHT as f64; 
    let angle = (std::f64::consts::PI * 0.5 * fov / 180.).tan();

    let xx = (2. * ((x as f64 + 0.5) * invWidth) - 1.) * angle * aspectratio; 
    let yy = (1. - 2. * ((y as f64 + 0.5) * invHeight)) * angle; 
    // let u = IMAGE_LEFT + x as f64 + 0.5;
    // let v = IMAGE_TOP - y as f64 + 0.5;

    // let focal = 1./((45./2.0_f64).tan());

    Ray { origin: cgmath::Vector3::new(0., 0., 0.),
          direction: cgmath::Vector3::new(xx, yy, -1.).normalize()
        }
}

pub fn ortho_camera_ray(x:usize, y:usize) -> Ray {
    let u = IMAGE_LEFT + x as f64 + 0.5;
    let v = IMAGE_TOP - y as f64 + 0.5;

    Ray { origin: cgmath::Vector3::new(u, v, 0.),
          direction: cgmath::Vector3::new(0., 0., 1.)
        }
}

fn set_pixel(buffer: &mut [u8], pitch: usize, x: usize, y:usize, color: Color) {
    let offset = y*pitch + x*3;
    buffer[offset] = color.r as u8;
    buffer[offset + 1] = color.g as u8;
    buffer[offset + 2] = color.b as u8;
}

fn shade_phong(
        light:          &Light, 
        cam_ray:        &Ray, 
        hit_pos:        &cgmath::Vector3<f64>, 
        surface_normal: &cgmath::Vector3<f64>, 
        specular_amount: f64,
    ) -> (f64, f64)
{
    // diffuse
    let L = (light.position - *hit_pos).normalize();
    let n_dot_l = light.intensity*(L.dot(*surface_normal).max(0.));

    // specular 
    let h = (L - cam_ray.direction).normalize();
    let n_dot_h = h.dot(*surface_normal);//.min(0.).max(1.);
    let spec = 
    // 0.;
    if n_dot_h > 0. {
        light.intensity*n_dot_h.powf(specular_amount)
    }
    else { 
        0. 
    };

    (n_dot_l, spec)
}
//use crate::lichtwerfer::*;

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
 
    let window = video_subsystem.window("rust-sdl2 demo", 800, 600)
        .position_centered()
        .build()
        .unwrap();
 
    let mut canvas = window.into_canvas().build().unwrap();
 
    canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 255, 255));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut i = 0;

    // let perspective = cgmath::frustum(-400.0, 400.0, -300.0, 300.0, 0.1, 1000.0); 
    // //     left:-400.0, right: 400.0, 
    // //     top: 300.0, bottom: -300.0, 
    // //     near: -0.1, far: -1000.0 
    // // };

    // let ray = Ray { origin: cgmath::Vector3::new(-400.0, 300.0, -1.0),
    //                 direction: perspective.transform_vector(cgmath::Vector3::new(-400.0, 300.0, 1.0))};
    // //let proj_ray = ray.direction);
    // println!("proj_ray: {}", ray);

    let materials = vec![
        Material { 
            diffuse: Color { r: 0.7, g: 0.1, b: 0.1 },
            specular: Color { r: 0.5, g: 0.5, b: 0.5 },
            // specular: Color { r: 0., g: 0., b: 0. },
            ambient: Color { r: 0.05, g: 0.05, b: 0.05 },
            spec_amount: 50.,
        },
        Material { 
            diffuse: Color { r: 0.2, g: 0.7, b: 0.1 },
            specular: Color { r: 0.5, g: 0.5, b: 0.5 },
            //specular: Color { r: 0., g: 0., b: 0. },
            ambient: Color { r: 0.05, g: 0.05, b: 0.05 },
            spec_amount: 20.,
        },
        Material { 
            diffuse: Color { r: 0.1, g: 0.2, b: 0.7 },
            specular: Color { r: 0.5, g: 0.5, b: 0.5 },
            //specular: Color { r: 0., g: 0., b: 0. },
            ambient: Color { r: 0.05, g: 0.05, b: 0.05 },
            spec_amount: 1000.,
        }
    ];

    let spheres = vec![
        Sphere::new( cgmath::Vector3::new(-0.5, -4., -32.), 3., 2),
        Sphere::new( cgmath::Vector3::new(-2., -1., -30.), 0.8, 0), 
        Sphere::new( cgmath::Vector3::new(2., -1., -30.), 0.8, 0), 
        Sphere::new( cgmath::Vector3::new(0., -1., -29.), 0.8, 1),
        ];

    let lights = vec![
        Light { 
            position: cgmath::Vector3::new(0., 100., 10.),
            intensity: 0.4, }, 
        Light { 
            position: cgmath::Vector3::new(-60., 60., 2.),
            intensity: 0.4, }, 
        Light { 
            position: cgmath::Vector3::new(60., 60., 20.),
            intensity: 0.4, }, 
        ];
    canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 0));
    canvas.clear();

    let texture_creator = canvas.texture_creator();

    let mut texture = texture_creator.create_texture_streaming(PixelFormatEnum::RGB24, 800, 600)
        .map_err(|e| e.to_string()).unwrap();

    texture.with_lock(None, |buffer: &mut [u8], pitch: usize| {
        //for y in 0..1 {
        let term = Term::stdout();

        term.write_line("0% Complete...");

        for y in 0..IMAGE_HEIGHT as usize {
            
            for x in 0..IMAGE_WIDTH as usize {
                
                let mut best_hit : Option<HitInfo> = None;
                let mut best_hit_idx : usize = 0;
                //let cam_ray = ortho_camera_ray(x, y);
                let cam_ray = camera_ray(x, y);
                //println!("Got ortho camera ray {}", cam_ray);

                // Check for intersect with each sphere in turn
                for i in 0..spheres.len() {
                    match intersect(&cam_ray, &spheres[i]) {
                        Some(hit) => {
                            // println!("Got a hit at: {}", hit.t);
                            best_hit = match best_hit {
                                Some(best_hit_val) => {
                                    if hit.t < best_hit_val.t {
                                        //println!("Got a better hit than before!");
                                        best_hit_idx = i;
                                        Some(hit)
                                    } else {
                                        Some(best_hit_val)
                                    }
                                },
                                None => {
                                    best_hit_idx = i;
                                    Some(hit)
                                }
                            };
                        },
                        _ => {
                            // println!("**No hit");
                        }
                    }
                }

                match best_hit {
                    Some(best_hit_val) => {
                        //println!("Hit sphere with t={}", best_hit_val.t);
                        let matIdx = spheres[best_hit_idx].matIdx;
                        let mat = materials[matIdx].clone();

                        let mut color = Color { 
                            r: (mat.ambient.r * 255.).min(255.),
                            g: (mat.ambient.g * 255.).min(255.),
                            b: (mat.ambient.b * 255.).min(255.),
                        };
                        
                        // Add up all of the lights in the scene.
                        for k in 0..lights.len() {
                            let light = &lights[k];

                            let light_vec = light.position - best_hit_val.reflect.origin;
                            let t_to_light = light_vec.magnitude();
                            let light_dir = light_vec.normalize();// / t_to_light;
                            let shadow_ray = Ray{ 
                                    origin: best_hit_val.reflect.origin + 0.01 * light_dir,
                                    direction: light_dir
                                };


                            let mut best_shadow_hit : Option<HitInfo> = None;
                            let mut best_shadow_hit_idx : usize = 0;

                            // Check for intersect with each sphere in turn
                            for i in 0..spheres.len() {

                                if i == best_hit_idx {
                                    continue;
                                }
                                match intersect(&shadow_ray, &spheres[i]) {
                                    Some(hit) => {
                                        // println!("Got a shadow hit at: {} with t_to_light = {}", hit.t, t_to_light);
                                        best_shadow_hit = match best_shadow_hit {
                                            Some(best_hit_val) => {
                                                if hit.t < best_hit_val.t || best_hit_val.t > t_to_light {
                                                    //println!("Got a better hit than before!");
                                                    Some(hit)
                                                } else {
                                                    best_shadow_hit_idx = i;
                                                    Some(best_hit_val)
                                                }
                                            },
                                            None => {
                                                //println!("Hit sphere {}, with t={}", i, hit.t);
                                                if hit.t < t_to_light {
                                                    best_shadow_hit_idx = i;
                                                    Some(hit)
                                                } else {
                                                    None
                                                }
                                            }
                                        }
                                    },
                                    _ => {
                                        // println!("**No hit");
                                    }
                                }
                            }

                            match best_shadow_hit {
                                None => {
                                    let (diffuse, specular) = 
                                        shade_phong(
                                            &light, 
                                            &cam_ray, 
                                            &best_hit_val.reflect.origin, 
                                            &best_hit_val.reflect.direction,
                                            mat.spec_amount
                                        );

                                    color.r = (color.r + (mat.specular.r * specular + mat.diffuse.r * diffuse)*255.0).min(255.);
                                    color.g = (color.g + (mat.specular.g * specular + mat.diffuse.g * diffuse)*255.0).min(255.);
                                    color.b = (color.b + (mat.specular.b * specular + mat.diffuse.b * diffuse)*255.0).min(255.);
                                }
                                _ => {}
                            }
                        }

                        set_pixel(buffer, pitch, x, y, color)
                    }, 
                    None => {
                        set_pixel(buffer, pitch, x, y, Color { r:0., g:0., b: 0.})
                    }
                }
            }
            term.clear_last_lines(1);
            let progress = format!("{:02.2}% Complete...", y as f64/IMAGE_HEIGHT*100.);
            term.write_line(&progress);
        }
        term.clear_last_lines(1);
        println!("Finished!")
    }).unwrap();

    canvas.clear();
    canvas.copy(&texture, None, Some(Rect::new(0,0, 800, 600))).unwrap();
    // canvas.copy_ex(&texture, None,
    //     Some(Rect::new(450, 100, 256, 256)), 30.0, None, false, false).unwrap();
    canvas.present();

    'running: loop {
        //i = (i + 1) % 255;
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                _ => {}
            }
        }

        //canvas.present();
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}
